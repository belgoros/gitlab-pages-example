# Example of GitLab Pages

This a simple example of building a static web-site using [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) using Tailwind CSS.

## Build locally

- Run `npx tailwindcss -i ./input.css -o ./output.css --watch` to watch your css changes. This will generate `output.css` file and will keep it up-to-date.
- Refresh the `index.html` page in your browser to see changes.
- once the changes are finished, commit and push your changes to the GitLab remote repo.
- this will trigger the CI/CD pipeline

Once the CI-CD pipeline is finished:

- go to the project at GitLab
- click on the `Settings -> Pages` section on the left side
- you will the URL to the deployed static web-site in the `Access pages' section: https://belgoros.gitlab.io/gitlab-pages-example.
